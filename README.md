# Butane config to host a Matrix homeserver on Fedora CoreOS

Example Butane config to host a Matrix homeserver on Fedora CoreOS. This will
setup:
  * nginx with Let's Encrypt for HTTPS support
  * Synapse with PostgreSQL and elements-web

For this setup, you just need a domain name, although you can optionally host
various services on customiseable subdomains, for example:
  * domain.tld
  * matrix.domain.tld
  * chat.domain.tld

For Let's Encrypt support, those domains must be configured beforehand to
resolve to the IP address that will be assigned to your server. If you do not
know what IP address will be assigned to your server in advance, you might want
to use another ACME challenge method to get Let's Encrypt certificates (see
[DNS Plugins][plugins]).

If you already have certificates from Let's Encrypt or another provider,
open an issue.

## How to use

To generate the Ignition configs, you need `make` and [Butane][butane]:

Then, you need to provide values for each variable in the secrets file:

```
$ cp modules/fedora-coreos-matrix/variables.sample.j2 variables.j2
$ ${EDITOR} variables.j2
# Fill in the values
```

### Configuring Dendrite

The Dendrite configuration requires a keyfile, you can generate it using the following command:

```
$ mkdir generated
$ podman run --rm --entrypoint="" \
      --security-opt label=disable \
      -v ./generated:/mnt \
      matrixdotorg/dendrite-monolith:latest \
      /usr/bin/generate-keys \
      -private-key /mnt/matrix_key.pem \
```
This command will generate `generated/matrix_key.pem`

### System and container updates

By default, Fedora CoreOS systems are updated automatically to the latest
released update. This makes sure that the system is always on top of security
issues (and updated with the latest features) wthout any user interaction
needed. The containers, as defined in the systemd units in the config, are
updated on each service startup. They will thus be updated at least once after
each system update as this will trigger a reboot approximately every two week.

To maximise availability, you can set an [update strategy][updates] in
Zincati's configuration to only allow reboots for updates during certain
periods of time.  For example, one might want to only allow reboots on week
days, between 2 AM and 4 AM UTC, which is a timeframe where reboots should have
the least user impact on the service. Make sure to pick the correct time for
your timezone as Fedora CoreOS uses the UTC timezone by default.

See this example config that you can add to your own `config.bu`:

```
[updates]
strategy = "periodic"

[[updates.periodic.window]]
days = [ "Mon", "Tue", "Wed", "Thu", "Fri" ]
start_time = "02:00"
length_minutes = 120
```

## Generate the ignition configuration

Finally, you can generate the final Ignition config with https://gitlab.com/hackintosh5/butane-compiler

```
$ python -m compiler
```

You are now ready to deploy your Fedora CoreOS Matrix home server.

## Deploying

See the [Fedora CoreOS docs][deploy] for instructions on how to use this
Ignition config to deploy a Fedora CoreOS instance on your prefered platform.

## Registering new users

Registration is disabled by default for security and to avoid mistakes. If you
want to create an instance with open registration, you can set the
`enable_registration` value to `true` in your `variables.j2` file.

Otherwise, you should use shared secret registration.

## PostgreSQL major version updates

Major PostgreSQL version updates require manual intervention to dump the
database with the current version and then import it in the new version. We
thus can not use the `latest` tag for this container image and manual
intervention will be required approximately once a year to update the PostreSQL
container version.

See this example to dump the current database and import it when moving from
version 13 to 14:

```
# Stop Synapse server to ensure no-one is writing to the database
$ systemctl stop synapse

# Dump the database
$ mkdir /var/srv/matrix/postgres.dump
$ cat /etc/postgresql_synapse
$ podman run --read-only --pod=matrix --rm --tty --interactive \
      -v /var/srv/matrix/postgres.dump:/var/data:z \
      docker.io/library/postgres:13 \
      pg_dump --file=/var/data/dump.sql --format=c --username=synapse \
      --password --host=localhost synapse

# Stop the PostgreSQL container
$ systemctl stop postgres

# Keep existing database as backup
$ mv /var/srv/matrix/postgres /var/srv/matrix/postgres.bak
$ mkdir /var/srv/matrix/postgres

# Edit the PostgreSQL unit to update the container version
$ vi /etc/systemd/system/postgres.service

# Start the new PostgreSQL container
$ systemctl start postgres

# Import the database. Make sure to use the new PostgreSQL container image
$ podman run --read-only --pod=matrix --rm --tty --interactive \
      -v /var/srv/matrix/postgres.dump:/var/data:ro,z \
      docker.io/library/postgres:14 \
      pg_restore --username=synapse --password --host=localhost \
      --dbname=synapse /var/data/dump.sql

# Start Synapse again
$ systemctl start synapse

# Cleanup once everything is confirmed working
$ rm -rf /var/srv/matrix/postgres.dump /var/srv/matrix/postgres.bak
```

[deploy]: https://docs.fedoraproject.org/en-US/fedora-coreos/getting-started/
[plugins]: https://certbot.eff.org/docs/using.html#dns-plugins
[updates]: https://coreos.github.io/zincati/usage/updates-strategy/#periodic-strategy
[butane]: https://coreos.github.io/butane/getting-started/#getting-butane
